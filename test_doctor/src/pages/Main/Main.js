import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { useDispatch, useSelector } from 'react-redux';
import Head from '../../components/Head/Head';
import BoxOrder from '../../components/BoxOrder/BoxOrder';
import Carusel from '../../components/Carusel/Carusel';
import Times from '../../components/Times/Times';
import DateItem from '../../components/DateItem/DateItem';
import {getDoctorsOperation} from '../../store/doctors/operations';
// import PropTypes from 'prop-types';
import './Main.scss';
import { getAllDoctors } from '../../store/doctors/selectors';

const Main = () => {
	const  dispatch = useDispatch();
	const doctors= useSelector(getAllDoctors);
	const [timeNumber, setTimeNumber] = useState(null);
	const [data, setData] = useState(null);
	const [order, setOrder] = useState({
		nameDoctor: '',
		dateUser: '',
		dateSystem: '',
		time: '',
	});
	useEffect(() => {
		axios.get('/times.json').then(res => {
			setTimeNumber(res.data);
		});
	}, []);
	useEffect(() => {
		dispatch(getDoctorsOperation())
		getDaysArray();
	}, [dispatch]);

	const getDaysArray = function () {
		let days = ['ВС', 'ПН', 'ВТ', 'СР', 'ЧТ', 'ПТ', 'СБ'];
		let date = new Date();
		const month = date.getMonth();
		const monthNext = month +1;
		let result = [];
		while (date.getMonth() === month ) {
			result.push({
				day: days[date.getDay()],
				dayNumber: date.getDate(),
				month: month,
			});
			date.setDate(date.getDate() + 1);
		}
		while (monthNext === date.getMonth(month+1) ) {
			result.push({
				day: days[date.getDay()],
				dayNumber: date.getDate(),
				month: monthNext,
			});
			date.setDate(date.getDate() + 1);
		}
		setData(result);
		return result;
	};

	if (doctors === undefined) {
		return <div>Подождите</div>;
	}

	const title = doctors.map((item, i) => (
		<Head
			order={order}
			setOrder={setOrder}
			key={item.name}
			name={item.name}
			time={item.time}
			img={item.img}
		/>
	));

	const days = data.map((item, i) => (
		<DateItem
			order={order}
			setOrder={setOrder}
			key={item.day}
			day={item.day}
			month={item.month}
			dayToday={i}
			dayNumber={item.dayNumber}
		/>
	));

	const timesDoc = timeNumber.map(item => (
		<Times order={order} setOrder={setOrder} time={item.time} />
	));
	console.log('🚀 ~ file: Main.js ~ line 17 ~ order', order);

	return (
		<div className="container_main">
			<Carusel slides={1} card={doctors} title={title} />
			<section className="container__date">
				<h4 className="littleHead"> Возможная дата</h4>
				<Carusel slides={3} multi={true} title={days} />
			</section>
			{timesDoc && (
				<section className="container__date">
					<h4 className="littleHead">Свободное время</h4>
					<Carusel slides={3} multi={true} title={timesDoc} />
				</section>
			)}
			<BoxOrder order={order} />
		</div>
	);
};

// Main.propTypes = {};

export default Main;

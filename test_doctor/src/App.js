import './App.scss';
import AppRouter from './routes/AppRouter/AppRouter';

function App() {
	return (
		<div>
			<AppRouter />
		</div>
	);
}

export default App;

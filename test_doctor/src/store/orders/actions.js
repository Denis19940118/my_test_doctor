import { ORDERS } from "./types";

export const saveOrders = orders => ({
    type: ORDERS,
    payload :  {allOrders: orders},
})
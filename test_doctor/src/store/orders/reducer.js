import { ORDERS } from "./types";

const initialState = {
    allOrders: []
};

const reducer = (state=initialState, action) => {
    switch(action.type){
        case ORDERS: {
            return {...state, allOrders:action.payload}
        } 
        default :{
            return state;
        }
    }
}

export default reducer;
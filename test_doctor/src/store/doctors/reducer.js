import { DOCTORS } from "./types";

const initialState = {
    allDoctors: []
};

const reducer = (state=initialState, action) => {
    switch(action.type){
        case DOCTORS: {
            return {...state, allDoctors:action.payload}
        } 
        default :{
            return state;
        }
    }
}

export default reducer;
import axios from 'axios';
import { saveDoctors } from "./actions";

export const getDoctorsOperation = () => async dispatch => {
  axios.get('/doctors.json').then(res => {
    
    dispatch(saveDoctors(res.data));
  });
};
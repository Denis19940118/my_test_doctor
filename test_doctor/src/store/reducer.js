import { combineReducers } from 'redux';
import doctorsReducer from './doctors/reducer';
import ordersReducer from './orders/reducer';

const reducer = combineReducers({
doctors:doctorsReducer,
orders:ordersReducer
});

export default reducer;

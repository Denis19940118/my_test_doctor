import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import ReactDOM from 'react-dom';
import store from './store/store';
import ErrorBoundary from './components/Error/ErrorBoundary/ErrorBoundary';
import App from './App';
import reportWebVitals from './reportWebVitals';
import './theme/styles/global.scss';
import '../node_modules/bootstrap/dist/css/bootstrap.css';

ReactDOM.render(
	<React.StrictMode>
		<Provider store={store}>
		<BrowserRouter>
			<ErrorBoundary>
				<App />
			</ErrorBoundary>
		</BrowserRouter>
		</Provider>
	</React.StrictMode>,
	document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();

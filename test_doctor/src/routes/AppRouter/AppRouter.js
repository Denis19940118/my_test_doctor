import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import Main from '../../pages/Main/Main';

const AppRouter = () => {
	return (
		<div>
			<Switch>
				<Redirect exact path="/" to="/main" />
				<Route exact path="/main">
					<Main />
				</Route>
			</Switch>
		</div>
	);
};

AppRouter.propTypes = {};

export default AppRouter;

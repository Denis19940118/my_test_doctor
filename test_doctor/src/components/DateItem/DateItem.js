import React, { useState } from 'react';
import PropTypes from 'prop-types';
import styles from './DateItem.module.scss';

const DateItem = props => {
	const [active, setActive] = useState('');
	const arrMonth = [
		'января',
		'февраля',
		'марта',
		'апреля',
		'мая',
		'июня',
		'июля',
		'августа',
		'сентября',
		'ноября',
		'декабря',
	];

	const clickItem = () => {
		if (active !== '') {
			props.setOrder({
				...props.order,
				dateUser: '',
				dateSystem: '',
				time: '',
			});
			setActive('');
		} else {
			setActive(styles.active);
			const year = new Date().getFullYear();
			const day = new Date(year, props.month, props.dayNumber);
			console.log('🚀 ~ file: DateItem.js ~ line 17 ~ clickItem ~ day', day);
			props.setOrder({
				...props.order,
				dateUser: props.dayNumber + ' ' + arrMonth[props.month],
				dateSystem: day,
			});
		}
	};

	return (
		<div
			key={props.key}
			className={`${styles.container__date} ${active}`}
			onClick={clickItem}
		>
			{props.dayToday === 0 && (
				<p className={`${styles.container__date_name} ${active}`}>Сегодня</p>
			)}
			{props.dayToday !== 0 && (
				<p className={`${styles.container__date_name} ${active}`}>
					{props.day}
				</p>
			)}
			<p className={`${styles.container__date_number} ${active}`}>
				{props.dayNumber}
			</p>
		</div>
	);
};

DateItem.propTypes = {
	props: PropTypes.object.isRequired,
};

export default DateItem;

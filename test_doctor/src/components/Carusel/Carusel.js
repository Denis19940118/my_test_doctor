import React from 'react';
import Slider from 'react-slick';
import PropTypes from 'prop-types';
import './Carusel.scss';

const Carusel = ({ slides, title }) => {
	const sliderSettings = {
		infinite: false,
		speed: 700,
		slidesToShow: slides,
		dots: false,
	};

	return (
		<>
			<Slider className="container__carousel" {...sliderSettings}>
				{title}
			</Slider>
		</>
	);
};
Carusel.propTypes = {
	slides: PropTypes.number.isRequired,
	title: PropTypes.oneOfType([PropTypes.element, PropTypes.array]).isRequired,
};

export default Carusel;

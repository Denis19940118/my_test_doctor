import React from 'react';
import PropTypes from 'prop-types';
import styles from './Button.module.scss';

const Button = ({
	backgroundColor,
	onClick,
	color,
	type,
	title,
	width,
	height,
	padd,
	className,
	disabled,
}) => {
	const styl = {
		color: color,
		backgroundColor: backgroundColor,
		minWidth: width,
		minHeight: height,
		padding: padd,
	};
	return (
		<button
			type={type}
			style={styl}
			className={className}
			onClick={onClick}
			disabled={disabled}
		>
			{title}
		</button>
	);
};

Button.propTypes = {
	onClick: PropTypes.func.isRequired,
	type: PropTypes.string.isRequired,
	title: PropTypes.string.isRequired,
};
Button.defaultProps = {
	className: styles.btn,
};

export default Button;

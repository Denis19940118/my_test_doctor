import React from 'react';
import PropTypes from 'prop-types';
import styles from './Container.module.scss';

const Container = ({ children, className }) => {
	return <div className={className}>{children}</div>;
};

Container.propTypes = {
	children: PropTypes.oneOfType([PropTypes.element, PropTypes.array])
		.isRequired,
	className: PropTypes.string.isRequired,
};

Container.default = {
	className: styles.container,
};

export default Container;

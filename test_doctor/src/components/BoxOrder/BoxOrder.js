import React from 'react';
import PropTypes from 'prop-types';
import Button from '../Button/Button';
import styles from './BoxOrder.module.scss';
import { useDispatch } from 'react-redux';
import { saveOrders } from '../../store/orders/actions';

const BoxOrder = props => {
	const dispatch = useDispatch()
	const { dateUser, time } = props.order;
    console.log("🚀 ~ file: BoxOrder.js ~ line 8 ~ order", props.order)

	const handleSubmit = () => {
		dispatch(saveOrders(props.order))
	};

	return (
		<div className={styles.container__order}>
			<div className={styles.container__order_wrapper}>
				<div className={styles.container__order_item}>
					<p className={styles.item_title}>Дата</p>
					<p className={styles.item_text}>{dateUser}</p>
				</div>
				<div className={styles.container__order_line}></div>
				<div className={styles.container__order_item}>
					<p className={styles.item_title}>Время</p>
					<p className={styles.item_text}>{time}</p>
				</div>
			</div>
			<div>
				<Button
					type="submit"
					className={styles.btn_order}
					onClick={handleSubmit}
					title="ЗАПИСАТЬСЯ НА БЕСПЛАТНУЮ ВСТРЕЧУ"
				/>
			</div>
		</div>
	);
};

BoxOrder.propTypes = {
	props: PropTypes.object.isRequired,
};

export default BoxOrder;

import React from 'react';
import PropTypes from 'prop-types';
import styles from './Times.module.scss';

const Times = props => {
	const clickTime = () => {
		props.setOrder({ ...props.order, time: props.time });
	};
	return (
		<p className={`${styles.time_text}`} onClick={clickTime}>
			{props.time}
		</p>
	);
};

Times.propTypes = {
	props: PropTypes.object.isRequired,
};

export default Times;

import React from 'react';
import man from '../../theme/image/man.png';
import woman from '../../theme/image/woman.png';
import PropTypes from 'prop-types';
import styles from './Head.module.scss';

const Head = props => {
	const handle = e => {
		console.log('🚀 ~ file: Head.js ~ line 12 ~ handle ~  e', e);
		props.setOrder({ ...props.order, nameDoctor: props.name });
	};
	return (
		<section className={styles.container__card} onTouchStart={handle}>
			<h2 className={styles.container__card_title}>{props.name}</h2>

			<div className={styles.container__card_wrapper}>
				<picture>
					{props.img === 'man' && (
						<img className={styles.container__card_img} src={man} />
					)}
					{props.img === 'woman' && (
						<img className={styles.container__card_img} src={woman} />
					)}
				</picture>
				<div className={styles.container__card_box}>
					<p className={styles.container__card_text}>
						Длительность консультации
					</p>
					<p className={styles.container__card_text_number}>
						{props.time} минут
					</p>
				</div>
			</div>
		</section>
	);
};

Head.propTypes = {
	props: PropTypes.object.isRequired,
};

export default Head;
